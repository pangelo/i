EESchema Schematic File Version 2  date Sun 09 Sep 2012 18:35:10 WEST
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:void_io
LIBS:i-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 3 3
Title ""
Date "9 sep 2012"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2550 2350 2550 2700
Connection ~ 1900 3850
Wire Wire Line
	1650 3850 3750 3850
Connection ~ 3250 3350
Wire Wire Line
	3250 3050 3250 3350
Wire Wire Line
	2550 2550 3750 2550
Wire Wire Line
	3750 3150 3750 3050
Connection ~ 2550 2550
Wire Wire Line
	3750 3850 3750 3550
Wire Wire Line
	2550 3200 2550 3350
Wire Wire Line
	2150 3350 1900 3350
Wire Wire Line
	2550 3350 2750 3350
Wire Wire Line
	3450 3350 3150 3350
Wire Wire Line
	3750 3050 3900 3050
$Comp
L CONN_1 A1
U 1 1 4FF4B6EE
P 1900 2800
F 0 "A1" H 1980 2800 40  0000 L CNN
F 1 "ANT" H 1900 2855 30  0001 C CNN
	1    1900 2800
	0    -1   -1   0   
$EndComp
$Comp
L C C3
U 1 1 4FF4B6ED
P 1900 3150
F 0 "C3" H 1950 3250 50  0000 L CNN
F 1 "120p" H 1950 3050 50  0000 L CNN
	1    1900 3150
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 4FF4B6EC
P 1900 3600
F 0 "R1" V 1980 3600 50  0000 C CNN
F 1 "10k" V 1900 3600 50  0000 C CNN
	1    1900 3600
	1    0    0    -1  
$EndComp
$Comp
L DIODE D1
U 1 1 4FF4B6EB
P 2350 3350
F 0 "D1" H 2350 3450 40  0000 C CNN
F 1 "1N6263" H 2350 3250 40  0000 C CNN
	1    2350 3350
	-1   0    0    1   
$EndComp
$Comp
L R R2
U 1 1 4FF4B6EA
P 2550 2950
F 0 "R2" V 2630 2950 50  0000 C CNN
F 1 "2M2" V 2550 2950 50  0000 C CNN
	1    2550 2950
	1    0    0    -1  
$EndComp
Text HLabel 2550 2350 1    60   Input ~ 0
+3V
$Comp
L C C4
U 1 1 4FF4B6E9
P 2950 3350
F 0 "C4" H 3000 3450 50  0000 L CNN
F 1 "10n" H 3000 3250 50  0000 L CNN
	1    2950 3350
	0    -1   -1   0   
$EndComp
$Comp
L NPN Q1
U 1 1 4FF4B6E8
P 3650 3350
F 0 "Q1" H 3650 3200 50  0000 R CNN
F 1 "2N4401" H 3650 3500 50  0000 R CNN
	1    3650 3350
	1    0    0    -1  
$EndComp
Text HLabel 1650 3850 0    60   Output ~ 0
GND
$Comp
L R R4
U 1 1 4FF4B6E7
P 3750 2800
F 0 "R4" V 3830 2800 50  0000 C CNN
F 1 "33k" V 3750 2800 50  0000 C CNN
	1    3750 2800
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 4FF4B6E6
P 3500 3050
F 0 "R3" V 3580 3050 50  0000 C CNN
F 1 "2M2" V 3500 3050 50  0000 C CNN
	1    3500 3050
	0    -1   -1   0   
$EndComp
$Comp
L CP1 C5
U 1 1 4FF4B6E5
P 4100 3050
F 0 "C5" H 4150 3150 50  0000 L CNN
F 1 "1u" H 4150 2950 50  0000 L CNN
	1    4100 3050
	0    -1   -1   0   
$EndComp
Text HLabel 4300 3050 2    60   Output ~ 0
AOUT
$EndSCHEMATC
