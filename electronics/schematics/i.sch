EESchema Schematic File Version 2  date Sun 09 Sep 2012 18:35:10 WEST
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:void_io
LIBS:i-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 1 3
Title "I-Box"
Date "9 sep 2012"
Rev "1"
Comp "void.io"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ 6200 1600
NoConn ~ 6200 1500
Wire Wire Line
	6600 1600 6400 1600
Wire Wire Line
	6400 1600 6400 1700
Wire Wire Line
	6400 1700 6200 1700
Wire Wire Line
	3250 1500 3950 1500
Wire Wire Line
	3950 2150 3950 2000
Wire Wire Line
	5500 2450 5500 2200
Wire Wire Line
	2100 1500 2550 1500
Wire Wire Line
	2550 1700 1150 1700
Wire Wire Line
	1150 1700 1150 1900
Wire Wire Line
	4750 1600 4250 1600
Wire Wire Line
	4250 1600 4250 2000
Wire Wire Line
	4250 2000 3950 2000
Wire Wire Line
	4100 1750 4100 1500
Wire Wire Line
	4100 1500 4750 1500
Wire Wire Line
	6200 1800 6600 1800
$Comp
L +12V #PWR01
U 1 1 4FF4B5FF
P 5400 2200
F 0 "#PWR01" H 5400 2150 20  0001 C CNN
F 1 "+12V" H 5400 2300 30  0000 C CNN
	1    5400 2200
	1    0    0    1   
$EndComp
NoConn ~ 4750 1700
$Comp
L SPEAKER SP1
U 1 1 4FF3BC11
P 6900 1700
F 0 "SP1" H 6800 1950 70  0000 C CNN
F 1 "SPEAKER" H 6800 1450 70  0000 C CNN
	1    6900 1700
	1    0    0    -1  
$EndComp
$Comp
L POT RV1
U 1 1 4FF3BB26
P 3950 1750
F 0 "RV1" H 3950 1650 50  0000 C CNN
F 1 "POT" H 3950 1750 50  0000 C CNN
	1    3950 1750
	0    1    1    0   
$EndComp
$Comp
L AMP A1
U 1 1 4FF3BAF6
P 5350 1350
F 0 "A1" H 5350 1350 60  0000 C CNN
F 1 "AMP" H 5350 1350 60  0000 C CNN
	1    5350 1350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 4FF3BA8A
P 5500 2450
F 0 "#PWR02" H 5500 2450 30  0001 C CNN
F 1 "GND" H 5500 2380 30  0001 C CNN
	1    5500 2450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 4FF3BA83
P 3950 2150
F 0 "#PWR03" H 3950 2150 30  0001 C CNN
F 1 "GND" H 3950 2080 30  0001 C CNN
	1    3950 2150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 4FF3B5DE
P 1150 1900
F 0 "#PWR04" H 1150 1900 30  0001 C CNN
F 1 "GND" H 1150 1830 30  0001 C CNN
	1    1150 1900
	1    0    0    -1  
$EndComp
$Sheet
S 2550 1250 700  650 
U 4FF3A8F6
F0 "i_allband_receiver_12v" 60
F1 "i_allband_receiver_12v.sch" 60
F2 "AOUT" O R 3250 1500 60 
F3 "GND" O L 2550 1700 60 
F4 "+12V" I L 2550 1500 60 
$EndSheet
$Comp
L SPST SW1
U 1 1 4FF37D2E
P 1600 1500
F 0 "SW1" H 1600 1600 70  0000 C CNN
F 1 "SPST" H 1600 1400 70  0000 C CNN
	1    1600 1500
	1    0    0    -1  
$EndComp
$Comp
L +12V #PWR05
U 1 1 4FF37B83
P 1100 1500
F 0 "#PWR05" H 1100 1450 20  0001 C CNN
F 1 "+12V" H 1100 1600 30  0000 C CNN
	1    1100 1500
	0    -1   1    0   
$EndComp
$EndSCHEMATC
