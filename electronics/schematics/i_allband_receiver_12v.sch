EESchema Schematic File Version 2  date Sun 09 Sep 2012 18:35:10 WEST
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:void_io
LIBS:i-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 2 3
Title ""
Date "9 sep 2012"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Connection ~ 4450 3300
Wire Wire Line
	4450 3300 4450 3700
Wire Wire Line
	4450 3700 3350 3700
Wire Wire Line
	4300 3300 4700 3300
Wire Wire Line
	3350 3700 3350 3400
Wire Wire Line
	2950 3100 2800 3100
Connection ~ 3350 3500
Wire Wire Line
	2950 3500 3750 3500
Wire Wire Line
	6000 3100 6250 3100
Wire Wire Line
	3750 3100 4700 3100
$Sheet
S 4700 2850 1300 950 
U 4FF4B693
F0 "i_allband_receiver_3v" 60
F1 "i_allband_receiver_3v.sch" 60
F2 "+3V" I L 4700 3100 60 
F3 "GND" O L 4700 3300 60 
F4 "AOUT" O R 6000 3100 60 
$EndSheet
$Comp
L LD33V U1
U 1 1 4FF3B3BA
P 3350 3150
F 0 "U1" H 3500 2954 60  0000 C CNN
F 1 "LD33V" H 3350 3350 60  0000 C CNN
	1    3350 3150
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 4FF3B3B9
P 2950 3300
F 0 "C1" H 3000 3400 50  0000 L CNN
F 1 "100n" H 3000 3200 50  0000 L CNN
	1    2950 3300
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 4FF3B3B8
P 3750 3300
F 0 "C2" H 3800 3400 50  0000 L CNN
F 1 "10u" H 3800 3200 50  0000 L CNN
	1    3750 3300
	1    0    0    -1  
$EndComp
Text HLabel 6250 3100 2    60   Output ~ 0
AOUT
Text HLabel 4300 3300 0    60   Output ~ 0
GND
Text HLabel 2800 3100 0    60   Input ~ 0
+12V
$EndSCHEMATC
