EESchema Schematic File Version 2  date Wed 11 Jul 2012 22:58:57 WEST
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:void_io
LIBS:i_allband_receiver_12v_build-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 1 2
Title ""
Date "11 jul 2012"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Connection ~ 6250 3900
Wire Wire Line
	6250 3900 6400 3900
Wire Wire Line
	6400 3900 6400 3550
Connection ~ 4450 3700
Wire Wire Line
	4450 3300 4450 4100
Wire Wire Line
	4450 4100 6250 4100
Wire Wire Line
	6250 4100 6250 3600
Connection ~ 4450 3300
Wire Wire Line
	4450 3700 3350 3700
Wire Wire Line
	3350 3700 3350 3400
Wire Wire Line
	2950 3100 2800 3100
Connection ~ 3350 3500
Wire Wire Line
	3750 3500 2950 3500
Wire Wire Line
	6000 3100 6250 3100
Wire Wire Line
	3750 3100 4700 3100
Wire Wire Line
	4700 3300 4200 3300
Wire Wire Line
	4200 3300 4200 3450
Wire Wire Line
	1800 3850 1800 3950
$Comp
L CONN_2 P2
U 1 1 4FFCBEA7
P 6750 3450
F 0 "P2" V 6700 3450 40  0000 C CNN
F 1 "SPKR" V 6800 3450 40  0000 C CNN
	1    6750 3450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 4FFCBE80
P 1800 3950
F 0 "#PWR01" H 1800 3950 30  0001 C CNN
F 1 "GND" H 1800 3880 30  0001 C CNN
	1    1800 3950
	1    0    0    -1  
$EndComp
$Comp
L +12V #PWR02
U 1 1 4FFCBE74
P 1800 3650
F 0 "#PWR02" H 1800 3600 20  0001 C CNN
F 1 "+12V" H 1800 3750 30  0000 C CNN
	1    1800 3650
	0    -1   -1   0   
$EndComp
$Comp
L CONN_2 P1
U 1 1 4FFCBE64
P 2150 3750
F 0 "P1" V 2100 3750 40  0000 C CNN
F 1 "PWR" V 2200 3750 40  0000 C CNN
	1    2150 3750
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG03
U 1 1 4FFCB97F
P 4200 3300
F 0 "#FLG03" H 4200 3395 30  0001 C CNN
F 1 "PWR_FLAG" H 4200 3480 30  0000 C CNN
	1    4200 3300
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG04
U 1 1 4FFCB967
P 1800 3100
F 0 "#FLG04" H 1800 3195 30  0001 C CNN
F 1 "PWR_FLAG" H 1800 3280 30  0000 C CNN
	1    1800 3100
	1    0    0    -1  
$EndComp
$Comp
L +12V #PWR05
U 1 1 4FF4C152
P 1800 3100
F 0 "#PWR05" H 1800 3050 20  0001 C CNN
F 1 "+12V" H 1800 3200 30  0000 C CNN
	1    1800 3100
	0    -1   -1   0   
$EndComp
$Comp
L SPST SW1
U 1 1 4FF4C2EF
P 2300 3100
F 0 "SW1" H 2300 3200 70  0000 C CNN
F 1 "SPST" H 2300 3000 70  0000 C CNN
	1    2300 3100
	1    0    0    -1  
$EndComp
$Comp
L POT RV1
U 1 1 4FF4C281
P 6250 3350
F 0 "RV1" H 6250 3250 50  0000 C CNN
F 1 "POT" H 6250 3350 50  0000 C CNN
	1    6250 3350
	0    1    1    0   
$EndComp
$Comp
L LD33V U1
U 1 1 4FF4C206
P 3350 3150
F 0 "U1" H 3500 2954 60  0000 C CNN
F 1 "LD33V" H 3350 3350 60  0000 C CNN
F 2 "TO-220" H 3350 3150 60  0001 C CNN
	1    3350 3150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 4FF4C17D
P 4200 3450
F 0 "#PWR06" H 4200 3450 30  0001 C CNN
F 1 "GND" H 4200 3380 30  0001 C CNN
	1    4200 3450
	1    0    0    -1  
$EndComp
$Sheet
S 4700 2850 1300 950 
U 4FF4B693
F0 "i_allband_receiver_3v" 60
F1 "i_allband_receiver_3v.sch" 60
F2 "+3V" I L 4700 3100 60 
F3 "GND" O L 4700 3300 60 
F4 "AOUT" O R 6000 3100 60 
$EndSheet
$Comp
L C C1
U 1 1 4FF3B3B9
P 2950 3300
F 0 "C1" H 3000 3400 50  0000 L CNN
F 1 "100n" H 3000 3200 50  0000 L CNN
	1    2950 3300
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 4FF3B3B8
P 3750 3300
F 0 "C2" H 3800 3400 50  0000 L CNN
F 1 "10u" H 3800 3200 50  0000 L CNN
	1    3750 3300
	1    0    0    -1  
$EndComp
$EndSCHEMATC
