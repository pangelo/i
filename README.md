I - Interactive Sound Installation
==================================

An interactive installation by Pedro Ângelo, Rebecca Moradalizadeh and Sónia Ralha, initiated in an artistic residency of digital arts, part of LCD in Guimarães, CAAA, (March 2012) and then developed in the same space.

The installation was showcased on a colective show of LCD in May, in CAAA and then in a final colective exhibition also from LCD, in July, in Praça 25 de Abril, Guimarães.

FINDlab, in Guimarães, part of CEC 2012, supported our work, by doing the acrylic boxes for the devices.

Overview
--------

The physical space we occupy is imperceptibly altered by the omnipresent media and technology we use. We are mediated in an invisible space filled with electromagnetic waves which connect us to one another – a human hyperlink.

Based on this idea of an invisible “nervous system”, the authors conceptualized an interactive installation where the visitor becomes a participant, unvoluntarily using his mobile phone on the collective composition of audio soundscapes.

Installation sketches
---------------------

![1 - Frontal view](images/sketch_front.jpg)

1 - Frontal view

![2 - Top view](images/sketch_top.jpg)

2 - Top view

First prototype
---------------

![](images/prototype_1.jpg)

![](images/prototype_2.jpg)

![](images/prototype_3.jpg)

Final version
-------------

![](images/final_1.jpg)

![](images/final_2.jpg)

![](images/final_3.jpg)
